﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define c = Character("Charles")

# The game starts here.

label start:
    $ You_Points = 0
    image gameshow = "gameshow.jpg"

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene gameshow

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.



    # These display lines of dialogue.

    c "LET'S PLAY A GAME SHALL WE?"

    c "THIS GAME, THIS SHOW, WELCOME CAUSE..."

    c "ARE YOU A iACADEMY GAME CHANGERS?"

    c "Are you ready?"

    menu:
        "Yes":
            jump soready

        "No":
            jump notready

    label soready:

        c "Cool then let the Games Begin!"
        jump Question1

    label notready:

        c "..."

        c "Typical"

    return

    label Question1:

        c "Question 1: iACADEMY stands for?"

        menu:
            "Information and Communications Academy":
                jump Answer1wrong
            "Informations and Communications of iAcademy":
                jump Answer1wrong
            "Information and Communication Technology Academy":
                $ You_Points += 1
                jump Answer1right
            "iACADEMY itself":
                jump Answer1wrong

        label Answer1right:
            c "The Answer is..."

            c "Correct!"

            c "Information and Communication Technology Academy for short iACADEMY"

            c "Next Question please."
            jump Question2

        label Answer1wrong:

            c "The Answer is..."

            c "Wrong!"

            c "It's stands for Information and Communication Technology Academy"

            c "On to the next question!"
            jump Question2

    label Question2:
        c "Question 2: When was iACADEMY Founded?"

        menu:
            "November 2001":
                $ You_Points += 1
                jump Answer2right
            "September 2001":
                jump Answer2wrong
            "October 2001":
                jump Answer2wrong
            "June 2001":
                jump Answer2wrong

        label Answer2right:
            c "The Answer is..."

            c "Correct!"

            c "It was on November 2001"

            c "Next Question please."
            jump Question3

        label Answer2wrong:

            c "The Answer is..."

            c "Wrong!"

            c "It was actually Founded in November 2001"

            c "It was only Established in June 2001"

            c "On to the next question!"
            jump Question3

    label Question3:
        c "Question 3: iACADEMY is the one of the first college institutions in the Philippines offering ________."

        menu:
            "BS Multimedia and Arts":
                jump Answer3wrong
            "BS Game Development":
                jump Answer3wrong
            "BS Software Engineering":
                jump Answer3wrong
            "BS Animation":
                $ You_Points += 1
                jump Answer3right

        label Answer3right:
            c "The Answer is..."

            c "Correct!"

            c "Animation is rarely offered in the Philippines"

            c "Next Question please."
            jump Question4

        label Answer3wrong:

            c "The Answer is..."

            c "Wrong!"

            c "what we are searching was which course was first in the Philippines that was still unknown for others"

            c "On to the next question!"
            jump Question4

    label Question4:
        c "Question 4: In 2010, it was appointed the first ______ in the ASEAN Region"

        menu:
            "Microsoft Center of Excellence":
                jump Answer4wrong
            "IBM Software Center of Excellence":
                $ You_Points += 1
                jump Answer4right
            "Software Engineering Center of Excellence":
                jump Answer4wrong
            "Lotus Academic Institute":
                $ You_Points += 1
                jump Answer4right

        label Answer4right:
            c "The Answer is..."

            c "Correct!"

            c "Both IBM Software Center of Excellence and the Lotus Academic Institute were appointed at the same Year"

            c "Next Question please."
            jump Question5

        label Answer4wrong:

            c "The Answer is..."

            c "Wrong!"

            c "Technically both IBM Software Center of Excellence and the Lotus Academic Institute were appointed at the same Year"

            c "On to the next question!"
            jump Question5

    label Question5:
        c "Question 5: When was iACADEMY moved to a new and bigger campus to house its growing student population?"

        menu:
            "2012":
                jump Answer5wrong
            "2013":
                jump Answer5wrong
            "2014":
                $ You_Points += 1
                jump Answer5right
            "2015":
                jump Answer5wrong

        label Answer5right:
            c "The Answer is..."

            c "Correct!"

            c "Next Question please."
            jump Question6

        label Answer5wrong:

            c "The Answer is..."

            c "Wrong!"

            c "it was 2014"

            c "On to the next question!"
            jump Question6

    label Question6:
        c "Question 6: Who was the first CEO of iACADEMY?"

        menu:
            "Monico V. Jacob":
                jump Answer6wrong
            "Eusebio Tanco":
                jump Answer6wrong
            "Vanessa L. Tanco":
                $ You_Points += 1
                jump Answer6right
            "Mitch Andaya":
                jump Answer6wrong

        label Answer6right:
            c "The Answer is..."

            c "Correct!"

            c "She is still the CEO and also the President of iACADEMY today!"

            c "Next Question please."
            jump Question7

        label Answer6wrong:

            c "The Answer is..."

            c "Wrong!"

            c "tsk tsk tsk, didn't reviewed the School huh? Correct answer is Vanessa L. Tanco"

            c "On to the next question!"
            jump Question7

    label Question7:
        c "You're Almost half way in the End"
        c "Question 7: What is the motto of iACADEMY"

        menu:
            "Dare to be different. Be a Game Changer":
                $ You_Points += 1
                jump Answer7right
            "Let's be different. Be the Game Changer":
                jump Answer7wrong
            "Dare us to be different. Game Changer!":
                jump Answer7wrong
            "Don't dare us to be different. Be a Game Changer":
                jump Answer7wrong

        label Answer7right:
            c "The Answer is..."

            c "Correct!"

            c "That's why be different and be a Game Changer!"

            c "Next Question please."
            jump Question8

        label Answer7wrong:

            c "The Answer is..."

            c "Wrong!"

            c "Are you even trying?"

            c "On to the next question!"
            jump Question8

    label Question8:
        c "Question 8: When was the iACADEMY Logo Replaced?"

        menu:
            "2012":
                jump Answer8wrong
            "2013":
                jump Answer8wrong
            "2014":
                jump Answer8wrong
            "2015":
                $ You_Points += 1
                jump Answer8right

        label Answer8right:
            c "The Answer is..."

            c "Correct!"

            c "is It?"

            c "Next Question please."
            jump Question9

        label Answer8wrong:

            c "The Answer is..."

            c "Wrong!"

            c "What are you doing? get a Grip!"

            c "On to the next question!"
            jump Question9

    label Question9:
        c "Question 9: iACADEMY holds its first 24-hour gaming competition entitled Battle League. When did it start?"

        menu:
            "2014":
                jump Answer9wrong
            "2015":
                $ You_Points += 1
                jump Answer9right
            "2016":
                jump Answer9wrong
            "2017":
                jump Answer9wrong

        label Answer9right:
            c "The Answer is..."

            c "Correct!"

            c "I'm Speechless..."

            c "Next Question please."
            jump Question10

        label Answer9wrong:

            c "The Answer is..."

            c "Wrong!"

            c "HAHAHA like you even know this!"

            c "On to the next question!"
            jump Question10

    label Question10:
        c "Question 10: iACADEMY is known having Industry Partners but some didnt stay long. Which company is still recongized as iACADEMY's Industry Partner"

        menu:
            "Microsoft":
                jump Answer10wrong
            "WACOM":
                $ You_Points += 1
                jump Answer10right
            "IBM":
                jump Answer10wrong
            "none of the above":
                jump Answer10wrong

        label Answer10right:
            c "The Answer is..."

            c "Correct!"

            c "Wow, I'm Impressed!"

            c "Next Question please."
            jump Question11

        label Answer10wrong:

            c "The Answer is..."

            c "Wrong!"

            c "GO REVIEW!!! BOOO"

            c "On to the next question!"
            jump Question11

    label Question11:
        c "Question 11: The second campus called “iACADEMY Nexus” was unveiled when?"

        menu:
            "February 11, 2018":
                jump Answer11wrong
            "February 12, 2018":
                $ You_Points += 1
                jump Answer11right
            "February 13, 2018":
                jump Answer11wrong
            "February 14, 2018":
                jump Answer11wrong

        label Answer11right:
            c "The Answer is..."

            c "Correct!"

            c "You really Are a...ehem"

            c "Next Question please."
            jump Question12

        label Answer11wrong:

            c "The Answer is..."

            c "Wrong!"

            c "have you been watching the News?"

            c "On to the next question!"
            jump Question12

    label Question12:
        c "Fill up the Blank part"
        c "Question 12: Oh my second home, I pay thee homage. And with Pride, I to the world Proclaim my ________."

        menu:
            "Iacademy":
                jump Answer12wrong
            "IACADEMY":
                jump Answer12wrong
            "iacademy":
                jump Answer12wrong
            "iACADEMY":
                $ You_Points += 1
                jump Answer12right

        label Answer12right:
            c "The Answer is..."

            c "Correct!"

            c "Sharp Eyes you have there *wink"

            c "Next Question please."
            jump Question13

        label Answer12wrong:

            c "The Answer is..."

            c "Wrong!"

            c "Use your Eyes! *sigh"

            c "On to the next question!"
            jump Question13

    label Question13:
        c "three more to goo"
        c "Question 13: What is the meaning of Nexus?"

        menu:
            "Intelligence":
                jump Answer13wrong
            "Technology":
                jump Answer13wrong
            "Communication":
                $ You_Points += 1
                jump Answer13right
            "Information":
                jump Answer13wrong

        label Answer13right:
            c "The Answer is..."

            c "Correct!"

            c "*clap *clap *clap"

            c "Next Question please."
            jump Question14

        label Answer13wrong:

            c "The Answer is..."

            c "Wrong!"

            c "hahahahahahahahahahahahahahahahahahahahahahahaahahahahahahahahahahahahahahahahaahhaahah"

            c "On to the next question!"
            jump Question14

    label Question14:
        c "Question 14: in School of Business There was a Third Course Identified before, do you know which one?"

        menu:
            "Real Estate Management":
                $ You_Points += 1
                jump Answer14right
            "Marketing Management":
                jump Answer14wrong
            "Financial Management":
                jump Answer14wrong
            "Management":
                jump Answer14wrong

        label Answer14right:
            c "The Answer is..."

            c "Correct!"

            c "Brilliant! you really Did your Research!"

            c "Next Question please."
            jump Question15

        label Answer14wrong:

            c "The Answer is..."

            c "Wrong!"

            c "Is it Hard? Sorry :("

            c "On to the next question!"
            jump Question15

    label Question15:
        c "LAST QUESTION"
        c "Final Question: The iACADEMY Academic Seal is composed of several elements that are significant to the institution which one is correct?"

        menu:
            "Torch":
                $ You_Points += 1
                jump Answer15right
            "Changing World":
                $ You_Points += 1
                jump Answer15right
            "City Skyline":
                $ You_Points += 1
                jump Answer15right
            "Shield":
                $ You_Points += 1
                jump Answer15right
            "Circles":
                $ You_Points += 1
                jump Answer15right
            "Rays":
                $ You_Points += 1
                jump Answer15right
            "Game":
                $ You_Points -= 5
                jump Answer15wrong

        label Answer15right:
            c "The Answer is..."

            c "Correct!"

            c "Well Everything is Correct EXCEPT Game.."

            c "That is it!"

            c "Lets see if you are a Game Changer!"
            jump FinalScore

        label Answer15wrong:

            c "The Answer is..."

            c "Wrong!"

            c "you know Game is the only element that is wrong..."

            c "That is it!"

            c "Lets see if you are a Game Changer!"
            jump FinalScore

    label FinalScore:
        c "drum roll..."

        c "You..."
    if You_Points > 10:
        c "PASSED! You really are a Game Changer!"
        c "Thanks for Playing!"
    else:
        c "Try Your Best Next Time :/"
        c "Thanks for Playing!"
        return
    return
